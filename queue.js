let collection = [];

// Write the queue functions below.


function print() {
  return collection;
}

// Function to add an element to the back of the queue (enqueue)
function enqueue(element) {
  if (!Array.isArray(collection)) {
      collection = []; 
    }

    var lastIndex = collection.length;
    collection[lastIndex] = element;
    return collection;
}

// Function to remove the front element from the queue (dequeue)
function dequeue() {
  if (!isEmpty()) {
    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    }
    collection.length--; 
  }
  return collection;
}


// Function to get the front element of the queue
function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0; 
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};